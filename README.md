> El Guapo: Would you say I have a *Plethora of Personal Logistics*?\
> Jefe: A what?\
> El Guapo: A *plethora*.\
> Jefe: Oh yes, you have a plethora.

*Plethora of Personal Logistics* can automagically fill out your personal logistics with items from your world while respecting your items' tweaks to the minimums and maximums.

The order of the items in the personal logistics isn't willy-nilly; it mimics the ordering that Factorio uses, but adds a blank row between categories since the interface isn't tabbed.

# Runtime settings

* **Populated items**: Controls which items are shown in broad terms--all of them, those currently craftable, or only personal logistics you've modified.
* **Maximum stack limit**: Sets the upper-bound for the pre-populated items in fractions of *stacks*, not items; less than zero for no limit.
* **Compacted**: Remove all empty cells during sorting.

**Populated item** can be cycled in-game with `ALT+CONTROL+P`, by default.

# Refreshing the sort order

Refreshes are pended by changing the runtime settings or when there are changes to known research. They are activated once you open your crafting window. Additionally, when mucking about with personal logistics, right-clicking on a cell, whether it's empty or not, will *immediately* re-sort the list.

# Known issues

Factorio imposes a limit of 1000 personal logistic slots. If you exceed this, choose a more restrictive **populated items** setting or **compact** the list.
