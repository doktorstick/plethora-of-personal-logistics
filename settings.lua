data:extend ({
    {
       type = "string-setting",
       name = "popl-which-items",
       setting_type = "runtime-per-user",
       default_value = "craftable",
       allowed_values = {"craftable", "all", "personal"},
       order = "a"
    },
    {
        type = "double-setting",
        name = "popl-max-default-stacks",
        setting_type = "runtime-per-user",
        default_value = -1,
        minimum_value = -1,
        order = "c"
    },
    {
       type = "bool-setting",
       name = "popl-compacted",
       setting_type = "runtime-per-user",
       default_value = false,
       order = "d"
    },
})
