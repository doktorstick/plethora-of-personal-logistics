local handler = require "event_handler"


-- NOTE: The slot_index is a `uint`, but experimentally the maximum
-- allowed value is 65536.
local SLOT_INDEX_MAX = 65536

-- The number of colums in the personal logistic gui.
local GUI_COLUMNS = 10

-- which-items setting constants.
local WI_CRAFTABLE = 1
local WI_ALL       = 2
local WI_PERSONAL  = 3
local WI_MAX       = 4

local which_items_map = {
   ["craftable"] = WI_CRAFTABLE,
   ["all"]       = WI_ALL,
   ["personal"]  = WI_PERSONAL,
}


local recipe_product_cache

local populate_recipe_product_cache = function()
   recipe_product_cache = {}

   local recipe_prototypes = game.get_filtered_recipe_prototypes{
      {filter="enabled", mode="and"},
      {filter="hidden", invert=true, mode="and"},
      {filter="has-product-item",
       elem_filters={{filter="flag", flag="hidden", invert=true}}
      },
   }
   for _, r in pairs (recipe_prototypes) do
      for _, p in pairs (r.products) do
         if p.type == "item" then
            if recipe_product_cache[p.name] == nil then
               recipe_product_cache[p.name] = { r.name }
            else
               table.insert (recipe_product_cache[p.name], r.name)
            end
         end
      end
   end
end


local refresh_personal_logistics = function (player)
   local rs = settings.get_player_settings (player.index)
   local max_default_stacks = rs["popl-max-default-stacks"].value
   local compacted = rs["popl-compacted"].value

   local which_items = which_items_map[rs["popl-which-items"].value]
   global.last_slot_index = global.last_slot_index or SLOT_INDEX_MAX

   if global.personal_params == nil then
      global.personal_params = {}

      for i = 1, global.last_slot_index do
         params = player.get_personal_logistic_slot (i)
         if params.name ~= nil then
            -- NOTE: This is stored in the params and updated
            -- after sorting so we can use it to remove personal
            -- overrides.
            params["slot_index"] = i
            global.personal_params[params.name] = params
         end
      end
   end

   if recipe_product_cache == nil then
      populate_recipe_product_cache()
   end

   local math_ceil = math.ceil
   local math_floor = math.floor

   local make_default = function (p)
      local stack_size = p.stack_size
      local max_count = math_ceil (stack_size * max_default_stacks)

      local params

      if p.order and p.order ~= "" and p.valid then
         params = {name=p.name}
         if max_default_stacks >= 0 then
            params["max"] = max_count
         end
      end

      return params
   end

   local personal_params = global.personal_params

   -- NOTE: To make the math a bit simpler, zero-based indexing
   -- is used, but the interface uses one-based indexing.
   local slot_index_0 = 0

   local item_prototypes = game.get_filtered_item_prototypes{
      {filter="flag", flag="hidden", invert=true},
   }

   -- NOTE: Currently the prototypes come back in the properly sorted
   -- order. We'll ride this for now since it's both simpler and
   -- saves some CPU cycles.

   -- table.sort (item_prototypes, function (u, v)
   -- )

   -- Because of this, we just use the name as the sole arbiter
   -- of switching (sub-)groups, as sometimes the subgroup.order
   -- is the same (e.g., stack-filter-inserter and pipe).

   local last_group_name = nil
   local last_subgroup_name = nil


   for _, p in pairs (item_prototypes) do
      local params = personal_params[p.name]
      local recipes = player.force.recipes

      if which_items == WI_ALL then
         if params == nil then
            params = make_default (p)
         end
      elseif which_items == WI_CRAFTABLE then
         local products_recipes_names = recipe_product_cache[p.name]
         if params == nil and products_recipes_names then
            for _, recipe_name in pairs (products_recipes_names) do
               local recipe = recipes[recipe_name]
               if recipe and recipe.enabled then
                  -- The product is part of a recipe known by this force.
                  params = make_default (p)
               end
            end
         end
      elseif which_items == WI_PERSONAL then
         -- Nothing to do.
      end

      if params ~= nil then
         if compacted then
            -- Nothing to do.
         elseif (last_group_name ~= nil and
             last_group_name ~= p.group.name) then
            local current_slot_index = slot_index_0

            -- Skip to the next row.
            slot_index_0 = math_ceil (slot_index_0 / GUI_COLUMNS) * GUI_COLUMNS
            -- And add a blank row.
            slot_index_0 = slot_index_0 + GUI_COLUMNS
            -- And wipe anything in between.
            for i = current_slot_index, slot_index_0-1 do
               player.clear_personal_logistic_slot (i+1)
            end

         elseif (last_subgroup_name ~= nil and
                 last_subgroup_name ~= p.subgroup.name) then
            local current_slot_index = slot_index_0

            -- Skip to the next row.
            slot_index_0 = math_ceil (slot_index_0 / GUI_COLUMNS) * GUI_COLUMNS
            -- And wipe anything in between.
            for i = current_slot_index, slot_index_0-1 do
               player.clear_personal_logistic_slot (i+1)
            end
         end

         local slot_index = slot_index_0 + 1
         if personal_params[p.name] then
               params["slot_index"] = slot_index
         end
         success = player.set_personal_logistic_slot (slot_index, params)
         slot_index_0 = slot_index

         last_group_name = p.group.name
         last_subgroup_name = p.subgroup.name
      end
   end

   for i = slot_index_0, global.last_slot_index-1 do
      player.clear_personal_logistic_slot (i+1)
   end
end


local on_cycle_which_items = function (event)
   local player = game.players[event.player_index]
   if player then
      local rs = settings.get_player_settings (player.index)
      local which_items = which_items_map[rs["popl-which-items"].value]
      local new_setting = which_items % (WI_MAX-1) + 1

      for k, v in pairs (which_items_map) do
         if v == new_setting then
            rs["popl-which-items"] = { value = k }
            break
         end
      end

      if player.character then
         refresh_personal_logistics (player)
         global.refresh_personal_logistics = false
      end
   end
end


local on_gui_opened = function (event)
   if (global.refresh_personal_logistics and
       event.gui_type == defines.gui_type.controller) then

      local player = game.players[event.player_index]
      if player and player.character then
         refresh_personal_logistics (player)
         global.refresh_personal_logistics = false
      end
   end
end


local on_entity_logistic_slot_changed = function (event)
   -- NOTE: event.entity.player throws an error "Entity is not a character."
   -- when trying to access .player, even to test for `nil`. This
   -- seems like a bug given the LuaEntity documentation.
   --
   -- Instead, we'll check for entity.type and avoid .player until we
   -- know it is a character.

   local who_changed = event.player_index
   local entity_changed = event.entity

   if who_changed ~= nil and entity_changed.type == "character" then
      -- Alright, a player has made a change and we've verified that
      -- it's the avatar of some player. Now make sure that that
      -- "some player" is the player that made the change, in case
      -- some mod allows one player to change logistics of another
      -- player character.
      if entity_changed.player == game.players[who_changed] then
         local player = entity_changed.player
         local slot_index = event.slot_index

         params = player.get_personal_logistic_slot (slot_index)
         if params.name == nil then
            -- If it was a personal override, remove it. Unfortunately
            -- we don't know what was removed so we have to search for it.
            for _, p in pairs (global.personal_params) do
               if p.slot_index == slot_index then
                  global.personal_params[p.name] = nil
               end
            end

            -- Forces a refresh now.
            refresh_personal_logistics (player)
         else
            params.slot_index = slot_index
            global.personal_params[params.name] = params
            -- Pends a refresh on next open (simply for sorting).
            global.refresh_personal_logistics = true
         end
      end
   end
end


local on_researched_change = function (event)
   global.refresh_personal_logistics = true
end


local on_setting_change = function (event)
   if event.setting_type == "runtime-per-user" then
      refresh_personal_logistics (game.players[event.player_index])
   end
end


local lib = {}


lib.events = {
   ["popl-cycle-which-items"] = on_cycle_which_items,
   [defines.events.on_gui_opened] = on_gui_opened,
   [defines.events.on_entity_logistic_slot_changed] = on_entity_logistic_slot_changed,

   [defines.events.on_research_finished] = on_researched_change,
   [defines.events.on_research_reversed] = on_researched_change,

   [defines.events.on_runtime_mod_setting_changed] = on_setting_change,
}


-- Called on new game.
-- Called when added to exiting game.
lib.on_init = function()
   global.last_slot_index = SLOT_INDEX_MAX
   global.personal_params = nil
   global.refresh_personal_logistics = true
end


-- Not called when added to existing game.
-- Called when loaded after saved in existing game.
lib.on_load = function()
end


-- Not called on new game.
-- Called when added to existing game.
lib.on_configuration_changed = function (config)
   global.refresh_personal_logistics = true
end


-- Register the events.
handler.add_lib (lib)
